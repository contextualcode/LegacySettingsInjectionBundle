<?php

namespace ContextualCode\LegacySettingsInjectionBundle;

use Symfony\Component\HttpKernel\Bundle\Bundle;

class ContextualCodeLegacySettingsInjectionBundle extends Bundle
{

    protected $name = "ContextualCodeLegacySettingsInjectionBundle";

}
